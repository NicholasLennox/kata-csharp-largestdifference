﻿using System;

namespace KataCSharp_LargestDifference
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] input1 = { 2, 3, 1, 7, 9, 5, 11, 3, 5 };
            int[] input2 = { 2, 3, 11, 7, 9, 5, 1, 3, 5 };
            Console.WriteLine(GetLargestDifference(input1));
            Console.WriteLine(GetLargestDifference(input2));
        }

        public static int GetLargestDifference(int[] input)
        {
            // Set up flags
            int globalMax = 0; // Largest difference overall
            int localMax = 0; // Largest difference for the current subset

            for (int i = 0; i < input.Length; i++)
            {
                // For every element - go from there to the end to find a local maximum
                for (int j = i; j < input.Length; j++)
                {
                    // Compare i'th element to j'th to find a difference
                    int difference = input[j] - input[i]; // Largest needs to be after smallest. If not true, it will be negative
                    // If the current difference 
                    if (difference > localMax)
                    {
                        localMax = difference;
                    }
                }
                // Compare local to global to find potential new global
                if(localMax > globalMax)
                {
                    globalMax = localMax;
                }
                // Checking and assignment can be cleaned up, this was done verbose to show individual steps
            }
            return globalMax;

        }
    }
}
